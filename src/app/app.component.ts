import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    const firebaseConfig = {
      apiKey: "AIzaSyArXMMCSpvKwplna0caY0DA8azwowDEfuI",
      authDomain: "bilblio-4023a.firebaseapp.com",
      databaseURL: "https://bilblio-4023a.firebaseio.com",
      projectId: "bilblio-4023a",
      storageBucket: "bilblio-4023a.appspot.com",
      messagingSenderId: "525602618134",
      appId: "1:525602618134:web:a93bd62bd6a9bf7b7cae37",
      measurementId: "G-VB88E07FV6"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
}
}
